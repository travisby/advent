#include <algorithm>
#include <cstdio>
#include <iostream>
#include <set>
#include <vector>

using namespace std;

int main() {
  vector<int> as, bs;

  int a, b;
  while (cin >> a >> b) {
    as.push_back(a);
    bs.push_back(b);
  }

  sort(as.begin(), as.end());
  sort(bs.begin(), bs.end());

  int score;
  for (int i = 0; i < as.size(); i++) {
    score += abs(as[i] - bs[i]);
  }
  cout << "part 1: " << score << endl;

  multiset<int> bss;
  for (auto b = bs.begin(); b != bs.end(); b++) {
    bss.insert(*b);
  }

  score = 0;
  for (auto a = as.begin(); a != as.end(); a++) {
    score += *a * bss.count(*a);
  }
  cout << "part 2: " << score << endl;
}
