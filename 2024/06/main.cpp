#include <iostream>
#include <ranges>
#include <set>
#include <stdexcept>
#include <utility>

using namespace std;

class Map {
public:
  Map(pair<int, int> bounds, set<pair<int, int>> obstacles)
      : bounds(bounds), obstacles(obstacles) {}
  bool obstacleAt(pair<int, int> position) {
    if (position.first < 0 || position.first >= bounds.first ||
        position.second < 0 || position.second >= bounds.second) {
      throw out_of_range("Position out of bounds");
    }

    return obstacles.find(position) != obstacles.end();
  }

private:
  set<pair<int, int>> obstacles;
  pair<int, int> bounds;
};

class Guard {
public:
  Guard(pair<int, int> position, char direction)
      : position(position), direction(direction) {}
  // visited includes a _direction_, because we can visit the same field
  // multiple times and it's not a loop as long as they're going in different
  // directions
  const set<pair<pair<int, int>, char>> getVisited() { return visited; }
  // returns false if the guard has left the ~building~ map
  // true otherwise
  bool advance(Map map) {
    pair<int, int> newPosition = position;
    switch (direction) {
    case 'v':
      newPosition.second++;
      break;
    case '^':
      newPosition.second--;
      break;
    case '<':
      newPosition.first--;
      break;
    case '>':
      newPosition.first++;
      break;
    }

    bool obstacle;
    try {
      obstacle = map.obstacleAt(newPosition);
    } catch (out_of_range) {
      // we have left the building!
      return false;
    }

    // if we find an obstacle...
    if (obstacle) {
      // then turn to the right
      switch (direction) {
      case '^':
        direction = '>';
        break;
      case '>':
        direction = 'v';
        break;
      case 'v':
        direction = '<';
        break;
      case '<':
        direction = '^';
        break;
      }
      // and try again
      // NOTE: we have _not_ updated position yet, which is intentional
      // we don't WANT to be on top of the obstacle
      return advance(map);
    }
    position = newPosition;

    if (visited.find(make_pair(position, direction)) != visited.end()) {
      throw runtime_error("Loop detected");
    }

    visited.insert(make_pair(position, direction));
    return true;
  }

private:
  pair<int, int> position;
  char direction;

  set<pair<pair<int, int>, char>> visited = {make_pair(position, direction)};
};

int main() {
  // we will use a set of obstacles
  // rather than an entire map
  // because the data is sparse; there's lots of "empty" fields
  // and we don't need to waste any data on them
  // we can just assume that if a field is not in the set, it's empty
  //
  // what problem DOES arrive from this, however
  // is that we still need a way to do bounds checking
  // as we read stdin, we'll keep an eye out on line length and line count
  set<pair<int, int>> obstacles;
  Guard *guard;

  int lineNo = 0;
  int charNo = 0;
  int lineLength = 0;

  for (char x = getchar(); x != EOF; x = getchar()) {
    switch (x) {
    case '\n':
      lineNo++;
      lineLength = charNo;
      // we're going to increment it once we're out of this switch statement, so
      // this will bring it to the correct value
      charNo = -1;
      break;
    case '.':
      break;
    case '#':
      obstacles.insert(make_pair(charNo, lineNo));
      break;
    case 'v':
    case '^':
    case '<':
    case '>':
      guard = new Guard(make_pair(charNo, lineNo), x);
      break;
    }
    charNo++;
  }

  // we can use the last line number and the last line length to determine the
  // bounds
  Map map(make_pair(lineNo, lineLength), obstacles);

  // we will make a copy of the guard; we want to preserve the initial state so
  // we can try lots of different permutations for P2 and we don't want to
  // bother implementing a reset() function for guard
  auto guardP1 = *guard;
  while (guardP1.advance(map))
    ;

  cout << "Part 1: " << guard->getVisited().size() << endl;

  // For part 2, we want to try every possible permutation of *one additional*
  // obstacle that could lead to a loop an optimization for this is knowing the
  // only useful place to add an obstacle would be where guardP1 already visited
  // so we'll iterate over *that* set, and try adding an obstacle in each spot.
  // nit: it's not valid to place an obstacle where the guard already is, so we
  // begin at visited++;
  //
  // The code below is not ideal; we make _a lot_ of copies of data
  int part2 = 0;
  set<pair<pair<int, int>, char>> visited = guardP1.getVisited();
  auto xs = visited | views::transform([](auto x) { return x.first; }) |
            ranges::to<set<pair<int, int>>>();

  for (auto x = ++xs.begin(); x != xs.end(); x++) {
    auto guardP2 = *guard;
    auto obstaclesP2 = obstacles;

    obstaclesP2.insert(*x);
    Map map2(make_pair(lineNo, lineLength), obstaclesP2);
    try {
      while (guardP2.advance(map2))
        ;
    } catch (runtime_error) {
      part2++;
    }
  }

  cout << "Part 2: " << part2 << endl;

  return 0;
}
