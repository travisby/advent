#include <algorithm>
#include <iostream>
#include <mdspan>
#include <ranges>
#include <string>
#include <vector>

using namespace std;

template <class T, class D, size_t N = D::rank()>
int countPatternInMdspan(mdspan<T, D> ms, vector<vector<array<int, N>>> offsets,
                         vector<char> match) {
  return ranges::count(
      // for each offset, convert to the string
      offsets | views::transform([ms](auto offset) {
        // but only if the coords are within the range!
        return offset | views::filter([ms](auto coord) {
                 for (int i = 0; i < ms.rank(); i++) {
                   if (coord[i] < 0 || coord[i] > ms.extent(i)) {
                     return false;
                   }
                 }
                 return true;
               }) |
               views::transform(
                   [ms](auto coord) { return ms[coord[0], coord[1]]; }) |
               ranges::to<vector<T>>();
      }),
      match);
}

int main() {
  // we'll need the size of our line later to determine column/rows
  int linesize = 0;

  // we'll store just characters, and use an mdspan over the 1D vec instead of
  // the typical 2D array
  vector<char> vec;

  string line;
  while (getline(cin, line)) {
    vec.reserve(vec.size() + line.size());
    for (char c : line) {
      vec.push_back(c);
    }

    // all lines should be the same, but we only have access to a "full" line in
    // the loop so just do it every time
    linesize = line.size();
  }
  vec.shrink_to_fit();

  mdspan ms = mdspan(vec.data(), vec.size() / linesize, linesize);

  // part 1
  int count = 0;
  for (int i = 0; i < ms.extent(0); i++) {
    for (int j = 0; j < ms.extent(1); j++) {
      // we only want to look for patterns when we're "ON" an XMAS
      // starting with the first character
      if (ms[i, j] != 'X') {
        continue;
      }

      vector<vector<array<int, 2>>> offsets = {
          //  X        M             A          S
          {{i, j}, {i + 1, j}, {i + 2, j}, {i + 3, j}},             // left
          {{i, j}, {i - 1, j}, {i - 2, j}, {i - 3, j}},             // right
          {{i, j}, {i, j - 1}, {i, j - 2}, {i, j - 3}},             // up
          {{i, j}, {i, j + 1}, {i, j + 2}, {i, j + 3}},             // up
          {{i, j}, {i + 1, j - 1}, {i + 2, j - 2}, {i + 3, j - 3}}, // upright
          {{i, j}, {i + 1, j + 1}, {i + 2, j + 2}, {i + 3, j + 3}}, // downright
          {{i, j}, {i - 1, j + 1}, {i - 2, j + 2}, {i - 3, j + 3}}, // downleft
          {{i, j}, {i - 1, j - 1}, {i - 2, j - 2}, {i - 3, j - 3}}, // upleft
      };

      count +=
          countPatternInMdspan(ms, offsets, vector<char>{'X', 'M', 'A', 'S'});
    }
  }

  cout << "part 1: " << count << endl;

  // part 2
  count = 0;
  for (int i = 0; i < ms.extent(0); i++) {
    for (int j = 0; j < ms.extent(1); j++) {
      // we only want to look for patterns when we're "ON" an MAS*MAS
      // starting with the middle character
      if (ms[i, j] != 'A') {
        continue;
      }

      vector<vector<array<int, 2>>> offsets = {
          //      M          A            S
          {{i - 1, j + 1}, {i, j}, {i + 1, j - 1}}, // upright
          {{i - 1, j - 1}, {i, j}, {i + 1, j + 1}}, // downright
          {{i + 1, j - 1}, {i, j}, {i - 1, j + 1}}, // downleft
          {{i + 1, j + 1}, {i, j}, {i - 1, j - 1}}, // upleft
      };

      // if we found two matches, we got'em
      if (countPatternInMdspan(ms, offsets, vector<char>{'M', 'A', 'S'}) == 2) {
        count++;
      }
    }
  }
  cout << "part 2: " << count << endl;
}
