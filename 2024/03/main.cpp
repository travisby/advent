#include <iostream>
#include <regex>

using namespace std;

int main() {
  regex pattern("(mul\\((\\d{1,3}),(\\d{1,3})\\)|do\\(\\)|don't\\(\\))");

  string line;
  int p1Sum = 0, p2Sum = 0;
  bool enabled = true;
  while (getline(cin, line)) {
    for (sregex_iterator it =
             sregex_iterator(line.begin(), line.end(), pattern);
         it != sregex_iterator(); it++) {
      smatch match = *it;

      if (match.str() == "do()") {
        enabled = true;
        continue;
      } else if (match.str() == "don't()") {
        enabled = false;
        continue;
      }

      p1Sum += stoi(match[2]) * stoi(match[3]);

      if (enabled) {
        p2Sum += stoi(match[2]) * stoi(match[3]);
      }
    }
  }

  cout << "part 1: " << p1Sum << endl;
  cout << "part 2: " << p2Sum << endl;
  return 0;
}
