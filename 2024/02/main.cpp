#include <cassert>
#include <iostream>
#include <ranges>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

bool safe(std::ranges::input_range auto levels) {
  // we need to know if this range is ascending or descending
  // but we don't want to require a forward_range to iterate multiple times
  // so we'll just  do something special on the first round of the loop
  bool *isAscending = nullptr;
  for (std::tuple<int, int> els : levels | views::adjacent<2>) {
    int a = std::get<0>(els);
    int b = std::get<1>(els);

    // is this our first round of the loop?  determine if we're ascending or
    // descending
    if (isAscending == nullptr) {
      isAscending = new bool(a < b);
    }

    int displacement = abs(a - b);
    if (displacement < 1 || displacement > 3 || (*isAscending && a > b) ||
        (!*isAscending && a < b)) {
      return false;
    }
  }

  // NOTE: if there was no input, we will return true still
  // if we wanted to ensure there was at least one pair, we could check
  // isAscending != nullptr
  return true;
}

int main() {
  int safeReportsP1 = 0;
  int safeReportsP2 = 0;

  for (string line; getline(cin, line);) {
    istringstream stream(line);
    vector<int> levels;

    int num;
    while (stream >> num) {
      levels.push_back(num);
    }

    // part 1
    if (safe(levels)) {
      safeReportsP1++;
    }

    // part 2
    // first, if it was _already safe_, then it's still safe
    if (safe(levels)) {
      safeReportsP2++;
      continue;
    }
    // next, we get one free mulligan.
    // we will try building up a range that skips a particular index
    // and iterate through every possible skip, until we either are safe or
    // determine the whole report to be unsafe
    for (int skipIndex = 0; skipIndex < levels.size(); skipIndex++) {
      // views::enumerate gives us the index, so we can skip a particular index
      if (safe(levels | views::enumerate |
               // skip that index
               views::filter([skipIndex](std::tuple<int, int> pair) {
                 return std::get<0>(pair) != skipIndex;
               }) |
               // and transform it back to just the value
               views::values)) {
        safeReportsP2++;
        break;
      }
    }
  }

  cout << "part 1: " << safeReportsP1 << endl;
  cout << "part 2: " << safeReportsP2 << endl;
  return 0;
}
