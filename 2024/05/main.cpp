#include <iostream>
#include <list>
#include <ranges>
#include <string>
#include <vector>

using namespace std;

template <typename T> class NonContiguousSortedRule {
public:
  NonContiguousSortedRule(T a, T b) {
    smaller = a;
    larger = b;
  }
  bool sorted(ranges::forward_range auto range)
    requires same_as<ranges::range_value_t<decltype(range)>, T>
  {
    // find just the two characters we care about
    auto x =
        range |
        views::filter([this](T i) { return i == smaller || i == larger; }) |
        ranges::to<vector<T>>();
    // if both were in there, we need to decide if this is sorted or not
    if (x.size() == 2) {
      return x[0] == smaller && x[1] == larger;
    }
    // if there was 0, or 1 matches, then we're fine!
    return true;
  }

  T getLarger() const { return larger; }

private:
  T smaller;
  T larger;
};

template <typename T> class ContiguouSorter {
public:
  void push_back(NonContiguousSortedRule<T> rule) { rules.push_back(rule); }
  bool sorted(ranges::forward_range auto range)
    requires same_as<ranges::range_value_t<decltype(range)>, T>
  {
    for (auto rule : rules) {
      if (!rule.sorted(range)) {
        return false;
      }
    }

    return true;
  }

  // the function signature around traits/constraints is cursed.  I asked
  // ChatGPT a lot on how we would write something that takes a range, while
  // also being writable to its end while also allowing us to remove an item in
  // the middle a list SHOULD be passed in here as the more performant data
  // structure, but any range that can satisfy this wild constraint is valid
  void sort(ranges::forward_range auto &range)
    requires(requires(
                remove_reference_t<decltype(range)> c,
                typename std::remove_reference_t<decltype(range)>::iterator it,
                typename remove_reference_t<decltype(range)>::value_type
                    value) {
              {
                c.erase(it)
              } -> same_as<
                  typename remove_reference_t<decltype(range)>::iterator>;
              { c.push_back(value) } -> std::same_as<void>;
            }) &&
            std::same_as<std::ranges::range_value_t<decltype(range)>, T>
  {

    // until we're fully sorted, find the first noncompliant rule and "fix" it
    // by pushing the offending value to the end of the range
    while (!sorted(range)) {
      auto noncompliant = *(rules | views::filter([range](auto rule) {
                              return !rule.sorted(range);
                            })).begin();
      for (auto it = range.begin(); it != range.end(); it++) {
        if (*it == noncompliant.getLarger()) {
          range.erase(it);
          range.push_back(*it);
          break;
        }
      }
    }
    return;
  }

private:
  vector<NonContiguousSortedRule<T>> rules;
};

int main() {
  ContiguouSorter<int> sorter;

  int part1 = 0;
  int part2 = 0;

  string line;
  // first read rules
  bool finishedRules = false;
  while (getline(cin, line)) {
    // first, see if we're in between the rules and the data
    if (line == "") {
      finishedRules = true;
      continue;
      // next, check if we're still reading rules
    } else if (!finishedRules) {
      int a, b;
      if (2 != sscanf(line.c_str(), "%d|%d", &a, &b)) {
        cerr << "Invalid rule: " << line << endl;
        return 1;
      }
      sorter.push_back(NonContiguousSortedRule<int>(a, b));
      continue;
    }
    // we've finished rules, start applying them
    auto updates = line | views::split(',') | views::transform([](auto sub) {
                     return string(sub.begin(), sub.end());
                   }) |
                   views::transform([](string s) { return stoi(s); });

    if (sorter.sorted(updates)) {
      auto numbers = updates | ranges::to<vector<int>>();
      part1 += numbers[numbers.size() / 2];
    } else {
      auto numbers = updates | ranges::to<list<int>>();
      sorter.sort(numbers);
      auto numbersVec = numbers | ranges::to<vector<int>>();
      part2 += numbersVec[numbersVec.size() / 2];
    }
  }

  cout << "Part 1: " << part1 << endl;
  cout << "Part 2: " << part2 << endl;
}
