#include <iostream>
#include <list>
#include <memory>

using namespace std;

class Block {
public:
  virtual bool empty() = 0;
  virtual long getId() = 0;
  virtual ~Block() = default;
  Block() = default;
  Block(int size) : size(size) {}

  int getSize() { return size; }

  void shrink(int bySize) {
    if (bySize > size) {
      throw "Cannot shrink by more than size";
    }
    size -= bySize;
  }

private:
  int size = 1;
};
class FileBlock : public Block {
public:
  FileBlock(long id) : Block(), id(id) {}
  FileBlock(long id, int size) : Block(size), id(id) {}
  bool empty() { return false; }
  long getId() { return id; }

private:
  long id;
};
class FreeBlock : public Block {
public:
  FreeBlock() : Block() {}
  FreeBlock(int size) : Block(size) {}

  bool empty() { return true; }
  long getId() { throw "Not implemented"; }
};
class Filesystem : public list<unique_ptr<Block>> {
public:
  void print() {
    for (auto &&block : *this) {
      for (int i = 0; i < block->getSize(); i++) {
        if (block->empty()) {
          cout << ".";
        } else {
          cout << block->getId();
        }
      }
    }
    cout << endl;
  }

  long checksum() {
    long sum = 0;

    long pos = 0;
    for (auto &&block : *this) {
      for (int i = 0; i < block->getSize(); i++) {
        if (!block->empty()) {
          sum += pos * block->getId();
        }
        pos++;
      }
    }
    return sum;
  }

  bool defrag() {
    for (auto potentialFileBlock = rbegin(); potentialFileBlock != rend();
         potentialFileBlock++) {
      // only operate on non-empty space
      if (potentialFileBlock->get()->empty()) {
        continue;
      }
      // cool, we've found our non-empty block.  Can we find a new empty block
      // to place it in?
      for (auto potentialFreeBlock = begin();
           potentialFreeBlock != next(potentialFileBlock).base();
           potentialFreeBlock++) {
        // only operate on empty space
        if (!potentialFreeBlock->get()->empty() ||
            potentialFreeBlock->get()->getSize() <
                potentialFileBlock->get()->getSize()) {
          continue;
        }

        // if they're the exact same size, perform a nice swap
        if (potentialFreeBlock->get()->getSize() ==
            potentialFileBlock->get()->getSize()) {
          iter_swap(potentialFileBlock, potentialFreeBlock);
        } else {
          // ... .otherwise, we need to split the free block
          // that involves a few operations:
          // 1. shrinking this free space
          // 2. moving the file block to JUST BEFORE the free space
          // 3. Adding a free space where the file block was
          //    - if we want to keep track of where the file block was, because
          //    we're using unique_ptr (and will need to use std::move)
          //    we'll need to store a copy to its position now
          int fileSize = potentialFileBlock->get()->getSize();
          auto filePosition = next(potentialFileBlock).base();

          // 1. shrinking the free space
          potentialFreeBlock->get()->shrink(fileSize);

          // 2. moving the file block to JUST BEFORE the free space
          // ... okay, it's not moving.  It's copying
          // because we still need to use `filePosition` in step 3, we can't
          // call erase() just yet
          insert(potentialFreeBlock, std::move(*potentialFileBlock));

          // 3. Adding a free space where the file block was
          insert(filePosition, make_unique<FreeBlock>(fileSize));

          // finally, we can remove the file block
          erase(filePosition);

          // XXX: And here, we could actually _combine_ contiguous free blocks
          // but that's not necessary for this problem
        }
        return true;
      }
    }
    return false;
  }
};

int main() {
  Filesystem fsp1, fsp2;

  long charNo = 0;
  while (char c = getchar()) {
    if (c == '\n' || c == EOF) {
      break;
    }

    int size = stoi(string(1, c));
    for (int i = 0; i < size; i++) {
      if (charNo % 2 == 0) {
        fsp1.push_back(make_unique<FileBlock>(charNo / 2));
      } else {
        fsp1.push_back(make_unique<FreeBlock>());
      }
    }

    if (charNo % 2 == 0) {
      fsp2.push_back(make_unique<FileBlock>(charNo / 2, size));
    } else {
      fsp2.push_back(make_unique<FreeBlock>(size));
    }

    charNo++;
  }

  while (fsp1.defrag()) {
  }
  cout << "Part 1: " << fsp1.checksum() << endl;

  // fsp2.print();
  while (fsp2.defrag()) {
    // fsp2.print();
  }
  cout << "Part 2: " << fsp2.checksum() << endl;
}
