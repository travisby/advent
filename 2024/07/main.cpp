#include <iostream>
#include <list>
#include <ranges>
#include <sstream>
#include <string>
using namespace std;

template <typename T, ranges::input_range Range>
  requires equality_comparable<T> && totally_ordered<T>
class Equation {

public:
  Equation(T test, Range values, bool part2)
      : test(test), values(values), part2(part2) {}
  T getTest() { return test; }

  bool possible() {
    return possible(*values.begin(), ++values.begin(), values.end());
  }

private:
  T test;
  Range values;
  bool part2;

  bool possible(T value, Range::const_iterator current,
                Range::const_iterator end) {
    if (current == end) {
      return value == test;
    }

    // if we're already > test, there's no point continuing on.
    if (value > test) {
      return false;
    }

    T cur = *current;
    current++;

    bool possibles = possible(value + cur, current, end) ||
                     possible(value * cur, current, end);

    if (part2) {
      possibles =
          possibles ||
          possible(stoull(to_string(value) + to_string(cur)), current, end);
    }
    return possibles;
  }
};

int main() {
  int64_t part1 = 0;
  int64_t part2 = 0;
  string line;
  while (getline(cin, line)) {
    // we convert the line to a stream
    // so we can "consume" from it, rather than setting up an iterator
    // ourselves, etc.
    istringstream stream(line);

    // the first value is always the test value
    int64_t test;
    stream >> test;

    stream.get(); // get the `:` and toss it

    // and after that, everything is just the list of numbers
    list<int64_t> nums;
    int num;
    while (stream >> num) {
      nums.push_back(num);
    }

    Equation eq1(test, nums, false);
    Equation eq2(test, nums, true);

    if (eq1.possible()) {
      part1 += eq1.getTest();
    }

    if (eq2.possible()) {
      part2 += eq2.getTest();
    }
  }

  cout << "Part 1: " << part1 << endl;
  cout << "Part 1: " << part2 << endl;
}
