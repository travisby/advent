# AoC 2024

This year the focus is going to be on CPP.
I do _not_ intend to keep allocations minimal, or pretend this is for microcontroller development.
I can already tell that two days in CPP can do a lot of hidden allocations! 

The goal is to learn more about the STL, and to get more comfortable with the language.
