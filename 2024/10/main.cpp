#include <iostream>
#include <list>
#include <mdspan>
#include <set>
#include <string>
#include <vector>

using namespace std;

// hike finds all peaks from a given position in the topography.
set<pair<int, int>> hike(mdspan<int, dextents<size_t, 2>> topo,
                         pair<int, int> position) {
  int height = topo[position.first, position.second];
  if (height == 9) {
    return set<pair<int, int>>{position};
  }

  set<pair<int, int>> peaks;
  for (auto [row, col] : {
           pair<int, int>{position.first - 1, position.second},
           pair<int, int>{position.first + 1, position.second},
           pair<int, int>{position.first, position.second - 1},
           pair<int, int>{position.first, position.second + 1},
       }) {
    if (row < 0 || row >= topo.extent(0) || col < 0 || col >= topo.extent(1)) {
      continue;
    }

    if (topo[row, col] == height + 1) {
      peaks.insert_range(hike(topo, {row, col}));
    }
  }

  return peaks;
}

// hikep2 finds all _the trails_ to peaks from a given position in the
// topography.
//
// we use a list in here because we want to combine lists, and that's easier
// with lists than vectors
set<list<pair<int, int>>> hikep2(mdspan<int, dextents<size_t, 2>> topo,
                                 pair<int, int> position) {
  int height = topo[position.first, position.second];
  if (height == 9) {
    return set<list<pair<int, int>>>{{position}};
  }

  set<list<pair<int, int>>> paths;
  for (auto [row, col] : {
           pair<int, int>{position.first - 1, position.second},
           pair<int, int>{position.first + 1, position.second},
           pair<int, int>{position.first, position.second - 1},
           pair<int, int>{position.first, position.second + 1},
       }) {
    if (row < 0 || row >= topo.extent(0) || col < 0 || col >= topo.extent(1)) {
      continue;
    }

    if (topo[row, col] == height + 1) {
      for (auto path : hikep2(topo, {row, col})) {
        path.push_front({row, col});
        paths.insert(path);
      }
    }
  }

  return paths;
}

int main() {
  string line;
  vector<int> v;
  int length;
  while (getline(cin, line)) {
    length = line.size();
    for (char ch : line) {
      v.push_back(ch - '0');
    }
  }

  mdspan<int, dextents<size_t, 2>> topo(v.data(), v.size() / length, length);

  // we use a vector because we're using an mdspan, and that uses a lot of
  // random access
  vector<pair<int, int>> trailheads;

  for (int row = 0; row < topo.extent(0); row++) {
    for (int col = 0; col < topo.extent(1); col++) {
      if (topo[row, col] == 0) {
        trailheads.push_back({row, col});
      }
    }
  }

  int part1 = 0;
  for (auto trailhead : trailheads) {
    part1 += hike(topo, trailhead).size();
  }
  cout << "Part 1: " << part1 << endl;

  int part2 = 0;
  for (auto trailhead : trailheads) {
    part2 += hikep2(topo, trailhead).size();
  }
  cout << "Part 2: " << part2 << endl;
}
