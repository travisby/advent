#include <iostream>
#include <map>
#include <set>
#include <string>

using namespace std;

// the reflectee is the antenna that is being reflected to another point on
// the map while the reflector is the mirror point
// resonance is how many "hops" away the antinode is
// a resonance of 0 is the reflectee itself
pair<int, int> reflection(pair<int, int> reflector, pair<int, int> reflectee,
                          int resonance) {

  return pair<int, int>(
      reflectee.first - resonance * (reflector.first - reflectee.first),
      reflectee.second - resonance * (reflector.second - reflectee.second));
}

int main() {
  string line;

  // we want to be able to look up all of our antannae by frequency (char)
  // we use a multimap because there will be multiple points with the same
  // frequency, and we want them all
  multimap<char, pair<int, int>> antennae;

  int y = 0;
  int x = 0;
  while (getline(cin, line)) {
    for (int i = 0; i < line.length(); i++) {
      if (line[i] != '.') {
        antennae.insert({line[i], {i, y}});
      }
    }
    x = line.length();
    y++;
  }

  // we want to keep track of all the antinodes for both part 1 and part 2
  // and we only care about _unique points_, we don't care about the specific
  // frequencies at a point so we're OK with just a set, rather than a multiset
  set<pair<int, int>> antinodeP1;
  set<pair<int, int>> antinodeP2;
  // let's iterate through the antennae
  for (auto reflectee = antennae.begin(); reflectee != antennae.end();
       reflectee++) {
    // find all of the OTHER nodes with the same frequency
    auto reflectors = antennae.equal_range(reflectee->first);
    for (auto reflector = reflectors.first; reflector != reflectors.second;
         reflector++) {
      if (reflector->second == reflectee->second) {
        continue;
      }

      // confusingly, for P2, antennae themselves are at resonance=0 and are
      // also antinodes
      // the condition for breaking is a little complicated, we'll break in-loop
      // rather than in the for signature
      for (int resonance = 0; true; resonance++) {
        auto reflected =
            reflection(reflector->second, reflectee->second, resonance);

        if (reflected.first < 0 || reflected.first >= x ||
            reflected.second < 0 || reflected.second >= y) {
          break;
        }

        // P1 only cares about the first hop (and _not_ the 0 hop either)
        if (resonance == 1) {
          antinodeP1.insert(reflected);
        }

        antinodeP2.insert(reflected);
      }
    }
  }

  cout << "Part 1: " << antinodeP1.size() << endl;
  cout << "Part 2: " << antinodeP2.size() << endl;

  return 0;
}
