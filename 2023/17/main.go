package main

import (
	"bufio"
	"container/heap"
	"fmt"
	"image"
	"log"
	"os"
)

type graph map[image.Point]uint8

// stolen from https://pkg.go.dev/container/heap@go1.21.5
type path struct {
	weight uint8
	p      []image.Point
}
type pathHeap []path

func (h pathHeap) Len() int           { return len(h) }
func (h pathHeap) Less(i, j int) bool { return h[i].weight < h[j].weight }
func (h pathHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }
func (h *pathHeap) Push(x any) {
	*h = append(*h, x.(path))
}

func (h *pathHeap) Pop() any {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func main() {
	var inF *os.File

	if len(os.Args) > 1 {
		var err error
		inF, err = os.Open(os.Args[1])

		if err != nil {
			log.Fatal(err)
		}
	} else {
		inF = os.Stdin
	}

	defer func() {
		if err := inF.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	scanner := bufio.NewScanner(inF)

	G := make(graph)

	y := 0
	maxX := 0
	for scanner.Scan() {
		for x, c := range scanner.Text() {
			temp := uint8(0)
			if n, err := fmt.Sscanf(string(c), "%d", &temp); err != nil {
				log.Fatal(err)
			} else if n != 1 {
				log.Fatal("bad scan")
			}
			G[image.Point{x, y}] = temp
			if maxX < x {
				maxX = x
			}
		}
		y++
	}
	lavaPool := image.Point{0, 0}
	machinePartsFactory := image.Point{maxX, y - 1}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// https://en.wikipedia.org/wiki/K_shortest_path_routing
	B := &pathHeap{}
	heap.Init(B)

	// P=empty,
	P := make([][]image.Point, 0)
	// countu = 0, for all u in V
	// TODO
	// insert path ps = {s} into B with cost 0
	heap.Push(B, path{0, []image.Point{lavaPool}})

	// while B is not empty and countt < K
	// TODO second condition
	for B.Len() > 0 {
		// let pu be the shortest cost path in B with cost C
		// B = B - {pu}, countu  = countu + 1
		// TODO count
		pu := B.Pop().(path)
		// if u = t then P = P U {pu}
		if pu.p[len(pu.p)-1] == machinePartsFactory {
			fmt.Printf("at the end\n")
		}

		_ = pu
	}

	_ = P
}
