#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

typedef struct point {
  int x;
  int y;
  unsigned char weight;
} point;

typedef struct path {
	int length;
	point *points;
} path;

// expects a graph of points, a source point, a sink point, and a path
// the path will be modified with the shortest path from source to sink
// expects path.points to be allocated to the size of the graph (worst case scenario)
void Dijkstra(path Graph, point source, point sink, path *A) {
}

// returns true if there is no more than 3 times in the same direction
bool SatisfiesCondition(path *A) {
	return false;
}

// expects a graph of points, a source point, a sink point, and a path
// the path will be modified with the shortest path from source to sink
// THAT SATISFIES DAY 17'S CONDITION: NO MORE THAN 3 TIMES IN THE SAME DIRECTION
// expects path.points to be allocated to the size of the graph (worst case scenario)
// it will be updated to the actual length of the returned path
void YenKSPButWithOnlyTheShortestConstraintedPathReturned(path Graph, point source, point sink, path *A) {
	Dijkstra(Graph, source, sink, A);

	// Initialize the set to store the potential next shortest path
	path *B = malloc(sizeof(A));

	// "for k from 1 to K" for us is while the condition is not satisfied
	while (!SatisfiesCondition(A)) {
		// The spur node ranges from the first node to the next to last node in the previous shortest path
		for (int i = 0; i < A->length - 2; i++) {
			point spurNode = A->points[i];
			// The sequence of nodes from the source to the spur node of the previous shortest path
			path rootPath = {i, A->points + 1};
		}

	}
}

int main() {
	point p[1000];
	path G = {0, p};

	int x = 0;
	int y = 0;
	for (char c = fgetc(stdin); c != EOF; c = fgetc(stdin)) {
		if (c == '\n') {
			y++;
			x = 0;
			continue;
		}
		G.points[G.length++] = (point){x, y, c - '0'};
		x++;
	}
	
	return 1;
}
