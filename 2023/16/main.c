#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define MAX_LINE_LENGTH 249
#define MAX_LINES 250

#ifdef PART1
#define END_ROW 0
// we use a negative number for end column to prevent us from ever trying that direction during part 1
#define END_COLUMN -1
#elifdef PART2
#define END_ROW numRows
#define END_COLUMN numColumns
#else
#error "PART1 or PART2 must be defined"
#endif

enum Direction : unsigned char {
	LEFT = 1,
	RIGHT = 2,
	UP = 4,
	DOWN = 8
};

// traverse fills in the `visited` array each time a point on the contraption is visited BY A DIRECTION
// this is both useful as the return value (e.g., we want to know how many spots were visited) AND as a cache to prevent redoing work (which is why it's a bitwise enum, rather than just a visited bool.. the direction matters)
void traverse(char (*)[MAX_LINE_LENGTH], int numRows, int numCols, int y, int x, enum Direction direction, enum Direction (*visited)[MAX_LINE_LENGTH]);

// resets visited to a blank slate so we can run this code lots and lots of times with the same visited buffer
void clearVisitors(enum Direction (*visited)[MAX_LINE_LENGTH], int numRows, int numCols);

int main() {
	char contraption[MAX_LINES][MAX_LINE_LENGTH];
	int numRows = 0;
	int numColumns = 0;
	while (fgets(contraption[numRows++], MAX_LINE_LENGTH, stdin) != NULL) {
		// assert we have a full line
		char *newline = strchr(contraption[numRows-1], '\n');
		assert (newline != NULL);
		assert (numRows <= MAX_LINES);

		newline[0] = '\0';
		numColumns = newline - contraption[numRows-1];
	}

	// assert that we didn't stop procesing early due to an error
	assert (!ferror(stdin));
	assert (feof(stdin));
	//
	// remove the last line, which is empty
	assert (strlen(contraption[numRows]) == 0);
	numRows--;

	// we track visited with a char instead of a bool
	// because we want to know what DIRECTION it was visited from
	// (e.g. was the light going from left to right)
	// we know light following the same path will end up taking the same path, so we can short-circuit
	// if we're on an existing path
	// we also need to track MULTIPLE directions, because it's possible we come from three different directions
	// all taking different paths afterwards -- so we'll use a bitmask with `Direction`
	enum Direction visited[MAX_LINES][MAX_LINE_LENGTH];

	int result = 0;
	for (int y = 0; y <= END_ROW; y++) {
		// clear visitors from previous run
		clearVisitors(visited, numRows, numColumns);
		// we need to start to the LEFT of (0,0) incase it's a '/', '\', or '|' and we start splitting off-the-bat
		int x = -1;
		enum Direction direction = RIGHT;

		traverse(contraption, numRows, numColumns, y, x, direction, visited);

		// finally, get the number of visited spots
		int calculation = 0;
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				calculation += visited[i][j] > 0;
			}
		}
		result = fmax(result, calculation);
	}

	for (int y = END_ROW; y >= 0; y--) {
		// clear visitors from previous run
		clearVisitors(visited, numRows, numColumns);
		// we need to start to the RIGHT of (END_ROW,0) incase it's a '/', '\', or '|' and we start splitting off-the-bat
		int x = -1;
		enum Direction direction = LEFT;

		traverse(contraption, numRows, numColumns, y, x, direction, visited);

		// finally, get the number of visited spots
		int calculation = 0;
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				calculation += visited[i][j] > 0;
			}
		}
		result = fmax(result, calculation);
	}

	for (int x = 0; x <= END_COLUMN; x++) {
		// clear visitors from previous run
		clearVisitors(visited, numRows, numColumns);
		// we need to start to the TOP of (0,0) incase it's a '/', '\', or '|' and we start splitting off-the-bat
		int y = -1;
		enum Direction direction = DOWN;

		traverse(contraption, numRows, numColumns, y, x, direction, visited);

		// finally, get the number of visited spots
		int calculation = 0;
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				calculation += visited[i][j] > 0;
			}
		}
		result = fmax(result, calculation);
	}

	for (int x = END_COLUMN; x >= 0; x--) {
		// clear visitors from previous run
		clearVisitors(visited, numRows, numColumns);
		// we need to start to the BOTTOM of (0,END_COLUMN) incase it's a '/', '\', or '|' and we start splitting off-the-bat
		int y = -1;
		enum Direction direction = UP;

		traverse(contraption, numRows, numColumns, y, x, direction, visited);

		// finally, get the number of visited spots
		int calculation = 0;
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numColumns; j++) {
				calculation += visited[i][j] > 0;
			}
		}
		result = fmax(result, calculation);
	}

	printf("%d\n", result);
}

void clearVisitors(enum Direction (*visited)[MAX_LINE_LENGTH], int numRows, int numCols) {
	for (int i = 0; i < numRows; i++) {
		for (int j = 0; j < numCols; j++) {
			visited[i][j] = 0;
		}
	}
}

void traverse(char (*contraption)[MAX_LINE_LENGTH], int numRows, int numCols, int y, int x, enum Direction direction, enum Direction (*visited)[MAX_LINE_LENGTH]) {
	// first, travel in the direction
	if (direction == LEFT) {
		x--;
	} else if (direction == RIGHT) {
		x++;
	} else if (direction == UP) {
		y--;
	} else if (direction == DOWN) {
		y++;
	} else {
		assert (false);
	}

	// are we out of bounds?
	if (x < 0 || x >= numCols || y < 0 || y >= numRows) {
		return;
	} 

	// have we been here before from this direction?
	// if we have, then we can safely exit; we will only follow an exact path
	// that we already have done before
	if (visited[y][x] & direction) {
		// we've been down this road before
		return;
	}

	// mark that this path has been visited by this direction
	// (the direction is important!)
	visited[y][x] |= direction;

	// okay, now determine next direction
	if (contraption[y][x] == '.') {
		// do nothing
	} else if (contraption[y][x] == '/') {
		if (direction == LEFT) {
			direction = DOWN;
		} else if (direction == RIGHT) {
			direction = UP;
		} else if (direction == UP) {
			direction = RIGHT;
		} else if (direction == DOWN) {
			direction = LEFT;
		}
	} else if (contraption[y][x] == '\\') {
		if (direction == LEFT) {
			direction = UP;
		} else if (direction == RIGHT) {
			direction = DOWN;
		} else if (direction == UP) {
			direction = LEFT;
		} else if (direction == DOWN) {
			direction = RIGHT;
		}
	} else if (contraption[y][x] == '|') {
		if (direction == LEFT || direction == RIGHT) {
			// we go both directions
			traverse(contraption, numRows, numCols, y, x, UP, visited);
			direction = DOWN;
		}
	} else if (contraption[y][x] == '-') {
		if (direction == UP || direction == DOWN) {
			// we go both directions
			traverse(contraption, numRows, numCols, y, x, LEFT, visited);
			direction = RIGHT;
		}
	} else {
		assert (false);
	}

	traverse(contraption, numRows, numCols, y, x, direction, visited);
}
