#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

// `expr $(while read line; do echo -n $line | wc -c; done < input_part2 | sort -n | tail -n1) + 2`
// one +1 to handle the \n
// another +1 to handle being a NULL terminated string, rather than needing to track size
#define MAX_LINE_LENGTH 162
#define MAX_ROWS 163
#define NUM_CYCLE_DETECTION_SIZE 100
#define TIME_TO_STABILIZE 10000

void tiltRocksNorth(char (*rocks)[MAX_LINE_LENGTH], int numRows, int numRocksInRow) {
	if (numRows < 2 || numRocksInRow < 0) {
		return;
	}

	for (int x = 0; x < numRocksInRow; x++) {
		for (int y = 0; y < numRows; y++) {
			// if there's an open space, find if any rocks between us and the end (or a #) can be moved
			if (rocks[y][x] != '.') {
				continue;
			}
			for (int nextY = y+1; nextY < numRows; nextY++) {
				if (rocks[nextY][x] == '#') {
					// we can't move any rocks, so we're done
					break;
				} else if (rocks[nextY][x] == 'O') {
					rocks[nextY][x] = '.';
					rocks[y][x] = 'O';
					break;
				}
			}
		}
	}

}

void tiltRocksEast(char (*rocks)[MAX_LINE_LENGTH], int numRows, int numRocksInRow) {
	for (int y = 0; y < numRows; y++) {
		for (int x = numRocksInRow; x >= 0; x--) {
			// if there's an open space, find if any rocks between us and the end (or a #) can be moved
			if (rocks[y][x] != '.') {
				continue;
			}
			for (int nextX = x-1; nextX >= 0; nextX--) {
				if (rocks[y][nextX] == '#') {
					// we can't move any rocks, so we're done
					break;
				} else if (rocks[y][nextX] == 'O') {
					rocks[y][nextX] = '.';
					rocks[y][x] = 'O';
					break;
				}
			}
		}
	}
}
void tiltRocksSouth(char (*rocks)[MAX_LINE_LENGTH], int numRows, int numRocksInRow) {
	if (numRows < 2 || numRocksInRow < 0) {
		return;
	}

	for (int x = 0; x < numRocksInRow; x++) {
		for (int y = numRows; y >= 0; y--) {
			// if there's an open space, find if any rocks between us and the end (or a #) can be moved
			if (rocks[y][x] != '.') {
				continue;
			}
			for (int nextY = y-1; nextY >= 0; nextY--) {
				if (rocks[nextY][x] == '#') {
					// we can't move any rocks, so we're done
					break;
				} else if (rocks[nextY][x] == 'O') {
					rocks[nextY][x] = '.';
					rocks[y][x] = 'O';
					break;
				}
			}
		}
	}
}
void tiltRocksWest(char (*rocks)[MAX_LINE_LENGTH], int numRows, int numRocksInRow) {
	for (int y = 0; y < numRows; y++) {
		for (int x = 0; x < numRocksInRow; x++) {
			// if there's an open space, find if any rocks between us and the end (or a #) can be moved
			if (rocks[y][x] != '.') {
				continue;
			}
			for (int nextX = x+1; nextX < numRocksInRow; nextX++) {
				if (rocks[y][nextX] == '#') {
					// we can't move any rocks, so we're done
					break;
				} else if (rocks[y][nextX] == 'O') {
					rocks[y][nextX] = '.';
					rocks[y][x] = 'O';
					break;
				}
			}
		}
	}
}

void spinCycle(char (*rocks)[MAX_LINE_LENGTH], int numRows, int numRocksInRow) {
	tiltRocksNorth(rocks, numRows, numRocksInRow);
	tiltRocksWest(rocks, numRows, numRocksInRow);
	tiltRocksSouth(rocks, numRows, numRocksInRow);
	tiltRocksEast(rocks, numRows, numRocksInRow);
	return;
}

int totalLoad(char (*rocks)[MAX_LINE_LENGTH], int numRows, int numRocksInRow) {
	int result = 0;

	for (int y = 0; y < numRows; y++) {
		for (int x = 0; x < numRocksInRow; x++) {
			if (rocks[y][x] == 'O') {
				result += numRows - y;
			}
		}
	}


	return result;
}

int main() {
	int result = 0;

	char rocks[MAX_ROWS][MAX_LINE_LENGTH];
	int numRows = 0;

	/*
	 *
	 */

	while (fgets(rocks[numRows++], MAX_LINE_LENGTH, stdin) != NULL) {
		assert (numRows < MAX_ROWS);
	}
	// skip the plain newline at the end of the file
	numRows--;
	// and skip the newline at the end of each line
	int numRocksInRow = strlen(rocks[0]) - 1;

#ifdef PART1
	tiltRocksNorth(rocks, numRows, numRocksInRow);
#elifdef PART2
	int cycle_detection[NUM_CYCLE_DETECTION_SIZE];
	for (int64_t i = 0; i < TIME_TO_STABILIZE + NUM_CYCLE_DETECTION_SIZE; i++) {
		spinCycle(rocks, numRows, numRocksInRow);
		if (i > TIME_TO_STABILIZE) {
			cycle_detection[i % NUM_CYCLE_DETECTION_SIZE] = totalLoad(rocks, numRows, numRocksInRow);
			for (int j = 0; j < NUM_CYCLE_DETECTION_SIZE; j++) {
				if (j != (i%NUM_CYCLE_DETECTION_SIZE) && cycle_detection[j] == cycle_detection[i % NUM_CYCLE_DETECTION_SIZE]) {
					printf("Answer is one of:\n");
					for (int64_t z = 0; z < (i%NUM_CYCLE_DETECTION_SIZE); z++) {
						printf("%d\n", cycle_detection[z]);
					}
					return 0;
				}
			}
		}
	}
	printf("Answer is one of:\n");
	for (int64_t i = 0; i < NUM_CYCLE_DETECTION_SIZE; i++) {
		printf("%d\n", cycle_detection[i]);
	}
#else
#error "Must define PART1 or PART2"
#endif

	/*
	for (int y = 0; y < numRows; y++) {
		for (int x = 0; x < numRocksInRow; x++) {
			printf("%c", rocks[y][x]);
		}
		printf("\n");
	}
	*/
	
	result = totalLoad(rocks, numRows, numRocksInRow);

	// assert that we didn't stop procesing early due to an error
	assert (!ferror(stdin));
	assert (feof(stdin));

	printf("%d\n", result);
}
