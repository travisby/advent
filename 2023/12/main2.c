#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>

#include <limits.h>

#define MAX_LINE_LENGTH 200
#define MAX_CHECKSUM_LENGTH 5

bool partiallyValid(char *record, int *checksums, int numChecksums) {
	if (numChecksums == 0) {
		return strchr(record, '#') == NULL;
	}

	// find the next checksum
	while (record[0] == '.' && strlen(record) > 0) {
		record++;
	}

	if (strlen(record) == 0) {
		return false;
	}

	// now, try to see if the checksum is valid
	int check = checksums[0];
	while (check > 0 && strlen(record) > 0) {
		if (*record == '#' || *record == '?') {
			check--;
		} else if (*record == '.') {
			break;
		} else {
			assert(false);
		}
		record++;
	}
	
	
	return check == 0 && partiallyValid(record, &checksums[1], numChecksums-1);
}

bool valid(char *record, int *checksums, int numChecksums) {
	return strchr(record, '?') == NULL && partiallyValid(record, checksums, numChecksums);
}

int possibilities(char *record, int currentChecksum, int *nextChecksums, int numChecksums) {
	if (numChecksums == 0) {
		return strchr(record, '#') == NULL;
	}

}

int main() {
	int result = 0;

	// buf holds (what should be) a whole line from stdin
	char buf[MAX_LINE_LENGTH];

	/*
	 */

	while (fgets(buf, MAX_LINE_LENGTH, stdin) != NULL) {
		// assert we have a full line
		assert (strchr(buf, '\n') != NULL);

		// convert the first space into a \0
		// so we have a full string that is just our input
		char *endOfRow = strchr(buf, ' ');
		assert (endOfRow != NULL);
		endOfRow[0] = '\0';

		int checksums[MAX_CHECKSUM_LENGTH];
		int numChecksums = 0;
		char *saveptr;
		for (char *token = strtok_r(endOfRow+1, ",", &saveptr); token != NULL; token = strtok_r(NULL, ",", &saveptr)) {
			// do something with token
			if (isdigit(token[0])) {
				checksums[numChecksums++] = atoi(token);
			}
		}

		// printf("Possibilities for %s: %d\n", buf, possibilities(buf, checksums, numChecksums));
		printf("Valid: %d\n", partiallyValid(buf, checksums, numChecksums));
		// result += possibilities(buf, checksums, numChecksums);
	}

	// assert that we didn't stop procesing early due to an error
	assert (!ferror(stdin));
	assert (feof(stdin));

	printf("%d\n", result);
}
