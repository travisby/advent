#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>

#include <limits.h>

#ifdef PART1
#define MAX_LINE_LENGTH 200
#define MAX_CHECKSUM_LENGTH 5
#elifdef PART2
#define MAX_LINE_LENGTH 20000
#define MAX_CHECKSUM_LENGTH 500
#endif

#if !defined(PART1) && !defined(PART2)
#error "Must define PART1 or PART2"
#endif

int possibilities(char *record, int *checksums, int numChecksums);

int main() {
	int result = 0;

	// buf holds (what should be) a whole line from stdin
	char buf[MAX_LINE_LENGTH];

	/*
	 * This was incredibly hard
	 * I kept wanting a recursive/bottom-up solution
	 * where we operate on each character
	 * but it became very difficult to track if we were "in" a block of checksums are not
	 * I modified the approach to be each "." is consumed in one go
	 * and a group of "#s" are consumed in one go
	 *
	 * it's only a valid possibility (thus incrementing 1 to our results) if we consume all of the checksums
	 * and there's no string left to read
	 *
	 * we'll add the results of if the str is a # or if the string is a . together, which is how we can
	 * account for the ? being either a # or a . (it helps both conditions, where normal known springs would only hit one)
	 *
	 * when we are consuming a block of #s we need to consume all of them, including ?s
	 * we consume all UNLESS the current checksum is at 0 AND it's a ?
	 * if so, that ? can be a `.`.  Otherwise we have to keep counting (even if checksum goes negative)
	 * if checksum went negative, end the recursion
	 * if checksum went to zero, continue recursing but starting at the next checksum
	 *
	 * this was difficult, but felt very rewarding
	 */

	while (fgets(buf, MAX_LINE_LENGTH, stdin) != NULL) {
		// assert we have a full line
		assert (strchr(buf, '\n') != NULL);

		// convert the first space into a \0
		// so we have a full string that is just our input
		char *endOfRow = strchr(buf, ' ');
		assert (endOfRow != NULL);

		endOfRow[0] = '\0';

		int checksums[MAX_CHECKSUM_LENGTH];
		int numChecksums = 0;
		char *saveptr;
		for (char *token = strtok_r(endOfRow+1, ",", &saveptr); token != NULL; token = strtok_r(NULL, ",", &saveptr)) {
			// do something with token
			if (isdigit(token[0])) {
				checksums[numChecksums++] = atoi(token);
			}
		}
#ifdef PART2
		assert (strlen(buf)*5 < MAX_LINE_LENGTH);
		int oldStrLen = strlen(buf);
		buf[oldStrLen++] = '?';
		for (int newStrLen = oldStrLen; newStrLen < oldStrLen*5; newStrLen+=oldStrLen) {
			memcpy(buf+newStrLen, buf, oldStrLen);
			buf[newStrLen+oldStrLen] = '\0';
		}
		// get rid of the last ?
		buf[strlen(buf)-1] = '\0';

		assert (numChecksums*5 < MAX_CHECKSUM_LENGTH);
		int newNumchecksums = numChecksums;

		for (int newNumChecksums = numChecksums; newNumChecksums < numChecksums*5; newNumChecksums+=numChecksums) {
			memcpy(checksums+newNumChecksums, checksums, numChecksums*sizeof(int));
		}
		numChecksums *= 5;

#endif

		printf("Completed 1\n");
		result += possibilities(buf, checksums, numChecksums);
		printf("Completed 1\n");
	}

	// assert that we didn't stop procesing early due to an error
	assert (!ferror(stdin));
	assert (feof(stdin));

	printf("%d\n", result);
}

int possibilities(char *record, int *checksums, int numChecksums) {
	int result = 0;
	int r = record[0];
	if (strlen(record) == 0) {
		return result + numChecksums == 0;
	}

	// both of these have || r == '?' AND += for the results
	// this is how we achieve testing both cases of what ? could be

	if (r == '.' || r == '?') {
		result += possibilities(record+1, checksums, numChecksums);
	}

	if (r == '#' || r == '?') {
		// if we found a damaged spring
		// and we think we're out of damaged springs... we're wrong
		if (numChecksums == 0) {
			return result;
		}
		// okay, now we need to consume all of the #s until this checksum is complete
		int checksum = checksums[0];
		checksums++;
		numChecksums--;
		while (strlen(record) > 0) {
			if (record[0] == '.') {
				break;
			}
			// IFF we're a ? and we're at 0, we can also skip
			// that means our '?' is a .
			if (checksum == 0 && record[0] == '?') {
				break;
			}
			checksum--;
			record++;
		}

		if (checksum != 0) {
			return result;
		}

		// XXX: How do we account for if the previous character was a '?' AND closed out the checksum that if there's ANOTHER '?' it must be a dot?????
		if (strlen(record) > 0 && record[0] == '?') {
			record++;
		}

		result += possibilities(record, checksums, numChecksums);
	}

	return result;
}
