#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>

#define MAX_LINE_LENGTH 100
#define MAX_POINTS 10000

#define UP_DOWN 1<<0
#define LEFT_RIGHT 1<<1

typedef struct {
	int x;
	int y;
	// 0 = undug
	// 1 = up/down
	// 2 = left/righ
	// 3 = all directions (gone over more than once)
	// 4 = "filled in", no direction (based on not being the trench, but being the inside of the trench)
	// we care about direction because we'll need it for the even-odd rule later
	unsigned char dug;
} point;

int compPoint (const void * a, const void * b) {
	point *p1 = (point *) a;
	point *p2 = (point *) b;
	if (p1->y == p2->y) {
		return p1->x - p2->x;
	} else {
		return p1->y - p2->y;
	}
}

int main() {
	char buf[MAX_LINE_LENGTH];
	int rows = 0;
	int columns = 0;

	point dig[MAX_POINTS];
	int x = 0;
	int y = 0;
	int numPoints = 0;
	int minX = INT_MAX;
	int maxX = INT_MIN;
	int minY = INT_MAX;
	int maxY = INT_MIN;

	while (fgets(buf, MAX_LINE_LENGTH, stdin) != NULL) {
		int num = atoi(buf+2);
		if (buf[0] == 'U') {
			for (int i = 0; i < num; i++) {
				--y;
				dig[numPoints++] = (point) {x, y, UP_DOWN};
			}
		} else if (buf[0] == 'D') {
			for (int i = 0; i < num; i++) {
				y++;
				dig[numPoints++] = (point) {x, y, UP_DOWN};
			}
		} else if (buf[0] == 'L') {
			for (int i = 0; i < num; i++) {
				--x;
				dig[numPoints++] = (point) {x, y, LEFT_RIGHT};
			}
		} else if (buf[0] == 'R') {
			for (int i = 0; i < num; i++) {
				++x;
				dig[numPoints++] = (point) {x, y, LEFT_RIGHT};
			}
		} else {
			assert (false);
		}


		assert (numPoints < MAX_POINTS);
	}


	// assert that we didn't stop procesing early due to an error
	assert (!ferror(stdin));
	assert (feof(stdin));

	// we'll be performing a binary search later  beacuse I don't want to make a hashmap
	// so we need to be sorted
	qsort(dig, numPoints, sizeof(point), compPoint);

	for (int i = 0; i < numPoints; i++) {
		minX = fmin(minX, dig[i].x);
		maxX = fmax(maxX, dig[i].x);
		minY = fmin(minY, dig[i].y);
		maxY = fmax(maxY, dig[i].y);
	}

	// assert we don't have any duplicate points
	for (int i = 1; i < numPoints; i++) {
		assert (!(dig[i].x == dig[i-1].x && dig[i].y == dig[i-1].y));
	}

	int result = 0;
	for (int y = minY; y <= maxY; y++) {
		bool odd = false;
		for (int x = minX; x <= maxX; x++) {
			bool dug = false;
			// if the point exists, then use it
			point *p = (point*) bsearch(&(point) {x, y, -1}, dig, numPoints, sizeof(point), compPoint);
			if (p == NULL) {
				// we're dug if we're inside the polygon made by trenches -- and the even-odd rule says we're inside the trench if we're an odd number of trenches away from the edge
				dug = odd;
			} else {
				// the trench itself is inside the polygon
				dug = true;

				// the even-odd rule is special when it comes to edges
				// (if you're going directly at the edge) then you check the... edge of the edges and see if they go in the same directions (e.g. L-----J both go up, where L-----7 go in different directions)
				// if they go in the same direction you add 2 (don't flipflop between odd/even); if they go in opposite directions you add 1 (flipflop between odd/even)
				// since our raycast goes LR we need to pay special attention to L/R edges
				if (p->dug & UP_DOWN) {
					odd = !odd;
				} else if (p->dug & LEFT_RIGHT) {
					// okay, what direction does the leftmost edge go?
					point *left = p;
					while (left->dug & LEFT_RIGHT) {
						point *q = left;
						q = (point*) bsearch(&(point) {left->x-1, left->y, -1}, dig, numPoints, sizeof(point), compPoint);
						if (q != NULL) {
							left = q;
						} else {
							break;
						}

					}
					bool leftGoesUpUp = ((point*) bsearch(&(point) {left->x, left->y-1, -1}, dig, numPoints, sizeof(point), compPoint) != NULL);

					// and the right?
					point *right = p;
					while (right->dug & LEFT_RIGHT) {
						point *q = right;
						q = (point*) bsearch(&(point) {right->x+1, right->y, -1}, dig, numPoints, sizeof(point), compPoint);
						if (q != NULL) {
							right = q;
						} else {
							break;
						}
					}
					bool rightGoesUpUp = ((point*) bsearch(&(point) {right->x, right->y+1, -1}, dig, numPoints, sizeof(point), compPoint) != NULL);

					if (leftGoesUpUp != rightGoesUpUp) {
						odd = !odd;
					}
				}
			}
			printf("%c", dug ? '#' : '.');

			result += dug;
		}
		printf("\n");
	}

	printf("%d\n", result);

	return 0;
}
