#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>

unsigned char hash(char *str) {
    unsigned char value = 0;
    for (int i = 0; i < strlen(str); i++) {
	    value = ((value + str[i])* 17) % 256;
    }

    return value;
}

typedef struct lens {
	int focalLength;
	struct lens *next;
	char id[10];
} lens;

typedef struct box {
	struct lens *lens;
} box;

// returns the parent of the lens if exists, otherwise returns what should become the next parent
// (the return value will be NOT NULL, but return->next will be null)
// if there is no lenses at all, OR the parent is b->lens, returns NULL
lens *findLensParent(box *b, char *id) {
	lens *parent = b->lens;
	if (b->lens == NULL) {
		return NULL;
	} else if (strcmp(b->lens->id, id) == 0) {
		return NULL;
	}

	for (lens *l = parent->next; l != NULL; l = l->next) {
		if (strcmp(l->id, id) == 0) {
			break;
		}
		parent = l;
	}

	return parent;
}

void removeLens(box *b, char *id) {
	lens *parent = findLensParent(b, id);
	if (parent == NULL && b->lens != NULL && strcmp(b->lens->id, id) == 0) {
		lens *old = b->lens;
		b->lens = old->next;
		free(old);
		return;
	}

	if (parent != NULL && parent->next != NULL) {
		lens *next = parent->next->next;
		free(parent->next);
		parent->next = next;
	}

}

void addOrReplaceLens(box *b, char *id, int focalLength) {
	lens *parent = findLensParent(b, id);
	if (parent == NULL && b->lens == NULL) {
		b->lens = malloc(sizeof(lens));
		b->lens->next = NULL;
		b->lens->focalLength = focalLength;
		strcpy(b->lens->id, id);
		return;
	} else if (parent == NULL) {
		lens temp;
		temp.next = b->lens;
		parent = &temp;
	} else if (parent->next == NULL) {
		parent->next = malloc(sizeof(lens));
		parent->next->next = NULL;
		parent->next->focalLength = focalLength;
		strcpy(parent->next->id, id);
		return;
	}

	parent->next->focalLength = focalLength;
	return;
}

int main() {
	int result = 0;

	char token[10];
	int numCharsInToken = 0;
#ifdef PART1
	for (char c = fgetc(stdin); c != EOF; c = fgetc(stdin)) {
		if (c == ',' || c == '\n') {
			token[numCharsInToken] = '\0';
			result += hash(token);
			numCharsInToken = 0;
			continue;
		}
		token[numCharsInToken++] = c;
	}
#elifdef PART2
	box boxes[256];
	for (int i = 0; i < 256; i++) {
		boxes[i].lens = NULL;
	}
	for (char c = fgetc(stdin); c != EOF; c = fgetc(stdin)) {
		if (c == '-') {
			token[numCharsInToken] = '\0';
			box *b = &boxes[hash(token)];
			removeLens(b, token);

		} else if (c == '=') {
			token[numCharsInToken] = '\0';
			box *b = &boxes[hash(token)];

			int focalLength = fgetc(stdin) - '0';
			addOrReplaceLens(b, token, focalLength);
		} else if (c == ',' || c == '\n') {
			numCharsInToken = 0;
		} else {
			token[numCharsInToken++] = c;
		}
	}

	for (int i = 0; i < 256; i++) {
		box b = boxes[i];
		lens *l = b.lens;
		int j = 0;
		while (l != NULL) {
			result += (i+1) * (j+1) * l->focalLength;
			lens *next = l->next;
			free(l);
			l = next;
			j++;
		}
	}


#else
#error "PART1 or PART2 must be defined"
#endif
	printf("%d\n", result);
}
